# gRPC & protobuf tech talk example
## (easy) Service communication with gRPC and protobuf

This repository contains the examples for the "(easy) Service communication with gRPC and protobuf" tech talk

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for demo purposes.

### Prerequisites

To run this examples you need the following available on your machine:
* Docker Engine
* Make

## Running the examples

### Running the server
```
cd server/grpc-tech-talk-server
make build
make run
```

### Running the clients

#### GoLang client
```
cd client/go
make build
SERVER_ADDR=<put here the IP:PORT given by the server> make run
```

#### Node.js client
```
cd client/js
make build
SERVER_ADDR=<put here the IP:PORT given by the server> make run
```

#### Python client
```
cd client/python
make build
SERVER_ADDR=<put here the IP:PORT given by the server> make run
```

## Development environment

### Server
```
cd server/grpc-tech-talk-server
make build
make dev
```
#### GoLang client
```
cd client/go
make build
make dev
```

#### Node.js client
```
cd client/js
make build
make dev
```

#### Python client
```
cd client/python
make build
make dev
```

## Built With

* [Docker](https://www.docker.com/) - Containerization
* [gRPC](https://grpc.io/) - A high-performance, open-source universal RPC framework
* [Protocol Buffers](https://developers.google.com/protocol-buffers/) - A language-neutral, platform-neutral extensible mechanism for serializing structured data
* [GoLang](https://golang.org/) - Open source programming language
* [Glide](https://glide.sh/) - Package Management for Go
* [Node.js](https://nodejs.org) - JavaScript runtime built on Chrome's V8 JavaScript engine
* [NPM](https://www.npmjs.com/) - Package Management for Node.js 
* [Python](https://www.python.org) - Open source programming language
* [PIP](https://pypi.org/project/pip/) - The PyPA recommended tool for installing Python packages.

## Authors

* **Joaquim Silva** - [joaquimjmsilva](https://bitbucket.org/joaquimjmsilva/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

