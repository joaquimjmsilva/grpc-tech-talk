package geodistance

import (
	"context"
	"time"

	_ "github.com/golang/protobuf/proto"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	pb "bitbucket.org/joaquimjmsilva/grpc-tech-talk-proto/gen/pb-go/proto"
	"github.com/buxtronix/golang/nmea/src/nmea"
	"github.com/umahmood/haversine"
)

type GeoDistanceService struct{}

func (s *GeoDistanceService) ConvertDMStoDD(
	ctx context.Context,
	req *pb.GeoPointConverterRequest,
) (*pb.GeoPointConverterResponse, error) {
	latlon, err := nmea.NewLatLong(req.Value)

	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	geoPoint := &pb.GeoPointConverterResponse{}
	geoPoint.Value = float64(latlon)

	return geoPoint, nil
}

func (s *GeoDistanceService) CalculateDistance(
	ctx context.Context,
	req *pb.CalculateLinearDistanceRequest,
) (*pb.CalculateLinearDistanceResponse, error) {
	// Point 1
	var point1 *pb.GeoPoint
	switch req.GetPoint1().(type) {
	case *pb.CalculateLinearDistanceRequest_Point1Dd:
		point1 = req.GetPoint1Dd()
	case *pb.CalculateLinearDistanceRequest_Point1Dms:
		p, err := s.convertDMStoGeoPoint(req.GetPoint1Dms())
		if err != nil {
			return nil, err
		}
		point1 = p
	}

	// Point 2
	var point2 *pb.GeoPoint
	switch req.GetPoint2().(type) {
	case *pb.CalculateLinearDistanceRequest_Point2Dd:
		point2 = req.GetPoint2Dd()
	case *pb.CalculateLinearDistanceRequest_Point2Dms:
		p, err := s.convertDMStoGeoPoint(req.GetPoint2Dms())
		if err != nil {
			return nil, err
		}
		point2 = p
	}

	switch req.Formula {
	case pb.Formula_HAVERSINE:
		return s.calculateDistanceHaversine(point1, point2)
	case pb.Formula_VINCENTY:
		return s.calculateDistanceVincenty(point1, point2)
	default:
		return nil, status.Error(codes.InvalidArgument, "invalid formula type")
	}
}

func (s *GeoDistanceService) convertDMStoGeoPoint(dms *pb.GeoPointDMS) (*pb.GeoPoint, error) {
	lat, err := nmea.NewLatLong(dms.Latitude)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "invalid point latitude")
	}
	lon, err := nmea.NewLatLong(dms.Longitude)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "invalid point longitude")
	}

	point := &pb.GeoPoint{}
	point.Latitude = float64(lat)
	point.Longitude = float64(lon)

	return point, nil
}

func (s *GeoDistanceService) calculateDistanceHaversine(
	point1 *pb.GeoPoint,
	point2 *pb.GeoPoint,
) (*pb.CalculateLinearDistanceResponse, error) {
	p1 := haversine.Coord{Lat: point1.Latitude, Lon: point1.Longitude}
	p2 := haversine.Coord{Lat: point2.Latitude, Lon: point2.Longitude}

	_, km := haversine.Distance(p1, p2)

	res := &pb.CalculateLinearDistanceResponse{}
	res.Distance = km * 1000 // convert to meters

	return res, nil
}

func (s *GeoDistanceService) calculateDistanceVincenty(
	point1 *pb.GeoPoint,
	point2 *pb.GeoPoint,
) (*pb.CalculateLinearDistanceResponse, error) {

	// Just for demo
	time.Sleep(6 * time.Second)

	return nil, status.Error(codes.Unimplemented, "Vincenty not implemented yet")
}
