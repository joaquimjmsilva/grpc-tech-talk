package env

import (
	"os"
	"testing"
)

const (
	envKey       = "TEST_ENV"
	envValue     = "RANDOM_VALUE"
	defaultValue = "DEFAULT_VALUE"
)

func teardown() {
	os.Unsetenv(envKey)
}

func TestSet(t *testing.T) {
	defer teardown()

	Set(envKey, envValue)
	val, ok := os.LookupEnv(envKey)

	if !ok {
		t.Errorf("Env key not defined: %s\n", envKey)
	} else if val != envValue {
		t.Errorf("Wrong env value\n\tExpected: %s\n\tActual: %s\n", envValue, val)
	}
}

func TestUnset(t *testing.T) {
	defer teardown()

	Set(envKey, envValue)

	Unset(envKey)

	_, ok := os.LookupEnv(envKey)

	if ok {
		t.Errorf("Env key should not be defined: %s\n", envKey)
	}

	Unset(envKey)
}

func TestLoad(t *testing.T) {
	defer teardown()

	os.Setenv(envKey, envValue)
	val, _ := Load(envKey)

	if val != envValue {
		t.Errorf("Expect loaded value to be (%s), but got (%s) instead", envValue, val)
	}
}

func TestLoadWithoutEnv(t *testing.T) {
	defer teardown()

	_, ok := Load(envKey)

	if ok {
		t.Errorf("Expect env (%s) to be defined", envKey)
	}
}

func TestLoadOrDefaultToReturnDefault(t *testing.T) {
	defer teardown()

	val := LoadOrDefault(envKey, defaultValue)

	if val != defaultValue {
		t.Errorf("Expect env to be (%s), but got (%s) instead", defaultValue, val)
	}
}

func TestLoadOrDefaultToReturnExistingEnv(t *testing.T) {
	defer teardown()

	os.Setenv(envKey, envValue)
	val := LoadOrDefault(envKey, defaultValue)

	if val != envValue {
		t.Errorf("Expect env to be (%s), but got (%s) instead", envValue, val)
	}
}

func TestLoadOrDefaultToEnsureEnvIsSet(t *testing.T) {
	defer teardown()

	loadedDefault := LoadOrDefault(envKey, defaultValue)
	loadedVal, _ := Load(envKey)

	if loadedDefault != loadedVal {
		t.Errorf("Env should be the same, got (%s) and (%s)", loadedDefault, loadedVal)
	}
}