package main

import (
	"context"
	"fmt"
	"grpc-tech-talk-server/env"
	"log"
	"net"
	"time"

	_ "github.com/golang/protobuf/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	pb "bitbucket.org/joaquimjmsilva/grpc-tech-talk-proto/gen/pb-go/proto"

	"grpc-tech-talk-server/service/geodistance"
)

func main() {
	port := env.LoadOrDefault("PORT", "5555")
	lis, err := net.Listen("tcp", ":" + port)
	if err != nil {
		panic(err)
	}

	geoDistance := new(geodistance.GeoDistanceService)

	grpcServer := grpc.NewServer(
		grpc.UnaryInterceptor(unaryInterceptor),
	)
	pb.RegisterGeoDistanceServer(grpcServer, geoDistance)

	reflection.Register(grpcServer)
	// List all possible server addresses
	printAddresses(port)

	if err := grpcServer.Serve(lis); err != nil {
		panic(err)
	}
}

// general unary interceptor function to handle auth per RPC call as well as logging
func unaryInterceptor(ctx context.Context,
	req interface{},
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler) (interface{}, error) {
	start := time.Now()

	// Here we could also implement authorization

	h, err := handler(ctx, req)

	//logging
	log.Printf("Request - Method:%s\tDuration:%s\tError:%v\n",
		info.FullMethod,
		time.Since(start),
		err,
	)

	return h, err
}

func printAddresses(port string) {
	ifaces, _ := net.Interfaces()
	// handle err
	for _, i := range ifaces {
		addrs, _ := i.Addrs()
		// handle err
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}

			fmt.Printf("Address: %s:%s\n", ip.String(), port)
		}
	}
}