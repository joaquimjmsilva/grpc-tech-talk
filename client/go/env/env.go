package env

import (
	"os"
)

func Set(env string, val string) {
	os.Setenv(env, val)
}

func Unset(env string) {
	os.Unsetenv(env)
}

func Load(env string) (string, bool) {
	return os.LookupEnv(env)
}

func LoadOrDefault(env string, def string) string {
	val, ok := Load(env)
	if !ok {
		os.Setenv(env, def)
		val = def
	}

	return val
}