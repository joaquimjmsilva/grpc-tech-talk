package main

import (
	"context"
	"fmt"
	"grpc-tech-talk-client/env"
	"log"

	pb "bitbucket.org/joaquimjmsilva/grpc-tech-talk-proto/gen/pb-go/proto"
	_ "github.com/golang/protobuf/proto"
	"google.golang.org/grpc"
)

func main() {
	serverAddr := env.LoadOrDefault("SERVER_ADDR", "")
	log.Println("Connecting to server: " + serverAddr)

	conn, err := grpc.Dial(serverAddr, grpc.WithInsecure())
	if err != nil {
		log.Fatal("unable to connect to server", err)
		return
	}
	defer conn.Close()

	client := pb.NewGeoDistanceClient(conn)

	ConvertDMStoDD(client)
}

func ConvertDMStoDD(client pb.GeoDistanceClient)  {
	fmt.Println("========= ConvertDMStoDD ==========")
	req := pb.GeoPointConverterRequest{
		Value: "39° 35' 23.3376\"",
	}

	fmt.Printf("Request: %#v\n", req)

	rsp, err := client.ConvertDMStoDD(context.Background(), &req)

	if err != nil {
		fmt.Println(err.Error())
		return
	}
	fmt.Printf("Response: %#v\n", rsp)
}
