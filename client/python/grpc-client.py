# coding=utf-8
import os
import grpc
import src.grpctechtalkproto.gen.pb_python.proto.geodistance_pb2_grpc as geodistance_pb2_grpc
from src.grpctechtalkproto.gen.pb_python.proto.geodistance_pb2 import GeoPointConverterRequest, \
    CalculateLinearDistanceRequest, GeoPoint, GeoPointDMS


def create_client():
    server_addr = os.environ['SERVER_ADDR']
    print('Server: ' + server_addr)
    channel = grpc.insecure_channel(server_addr)
    return geodistance_pb2_grpc.GeoDistanceStub(channel)


def demo_convert_dms_to_dd(stub):
    print("-------------- ConvertDMStoDD --------------")
    dms = GeoPointConverterRequest()
    dms.value = "39° 35' 23.3376\""
    print("Request:", dms)
    print("Response:", stub.ConvertDMStoDD(dms))


def create_calculate_distance_request(formula):
    req = CalculateLinearDistanceRequest()
    req.formula = formula

    point1 = GeoPoint()
    point1.latitude = 56.071269
    point1.longitude = 36.862564

    req.point1dd.CopyFrom(point1)

    point2 = GeoPointDMS()
    point2.latitude = "56° 13' 5.376\""
    point2.longitude = "35° 48' 28.3572\""

    req.point2dms.CopyFrom(point2)
    return req


def demo_calculate_distance_haversine(stub):
    print("-------------- CalculateDistance Haversine --------------")
    req = create_calculate_distance_request(0)
    print("Request:", req)
    print("Response:", stub.CalculateDistance(req))


def demo_calculate_distance_vincenty(stub):
    print("-------------- CalculateDistance Vincenty --------------")
    try:
        req = create_calculate_distance_request(1)
        print("Request:", req)
        print("Response:", stub.CalculateDistance(req))
    except grpc.RpcError as e:
        print("An error occurred:", e)


def run():
    stub = create_client()

    # Call ConvertDMStoDD
    demo_convert_dms_to_dd(stub)

    # Call CalculateDistance
    demo_calculate_distance_haversine(stub)
    demo_calculate_distance_vincenty(stub)


if __name__ == '__main__':
    run()
