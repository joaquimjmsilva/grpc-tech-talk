const client = require('./src/client')

const getMetadata = () => {
    // Timeout of 5 seconds
    return {
        deadline: new Date(Date.now() + 5000),
    }
}

const dms = "39° 35' 23.3376\""

// Call ConvertDMStoDD
const convertDMStoDDRequest = {value: dms}
client.ConvertDMStoDD(convertDMStoDDRequest, getMetadata(), (err, rsp) => {
    console.log('=============ConvertDMStoDD=============')
    console.log('Request:', convertDMStoDDRequest)
    if (err) {
        console.log('An error occurred', err, '\n')
    } else {
        console.log('Response:', rsp, '\n')
    }
})

// Call CalculateDistance with Haversine
const calculateDistanceRequest = {
    formula: 0,
    point1dd: {
        latitude: 56.071269,
        longitude: 36.862564
    },
    point2dms: {
        latitude: "56° 13' 5.376\"",
        longitude: "35° 48' 28.3572\""
    }
}
client.CalculateDistance(calculateDistanceRequest, getMetadata(), (err, rsp) => {
    console.log('=============CalculateDistance=============')
    console.log('Request:', calculateDistanceRequest)
    if (err) {
        console.log('An error occurred', err, '\n')
    } else {
        console.log('Response:', rsp, '\n')
    }
})

// Call CalculateDistance with Vincenty (Is not implemented and has a sleep of 6 seconds)
let calculateDistanceRequestVincenty = Object.assign({}, calculateDistanceRequest)
calculateDistanceRequestVincenty.formula = 1

client.CalculateDistance(calculateDistanceRequestVincenty, getMetadata(), (err, rsp) => {
    console.log('=============CalculateDistance=============')
    console.log('Request:', calculateDistanceRequestVincenty)
    if (err) {
        console.log('An error occurred', err, '\n')
    } else {
        console.log('Response:', rsp, '\n')
    }
})
