const path = require('path')
const grpc = require('grpc')
const protoLoader = require('@grpc/proto-loader')

const PROTO_PATH = path.join(__dirname, '../node_modules/grpc-tech-talk-proto/proto/geodistance.proto')

const packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    }
)

console.log(`Server Address: ${process.env.SERVER_ADDR}`)

const SERVER_ADDR = process.env.SERVER_ADDR || 'localhost:5555'

let geodistanceService;

const getClient = () => {
    if (!geodistanceService) {
        const protoDescriptor = grpc.loadPackageDefinition(packageDefinition)
        const geodistancePackage = protoDescriptor.com.joaquimjmsilva.geodistance
        geodistanceService = new geodistancePackage.GeoDistance(
            SERVER_ADDR,
            grpc.credentials.createInsecure()
        );
    }

    return geodistanceService
}

module.exports = getClient()